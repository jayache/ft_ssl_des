# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sbelondr <sbelondr@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/21 14:49:02 by jayache           #+#    #+#              #
#    Updated: 2022/02/02 10:54:38 by selver           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# ---------- #
# Debug mode #
# ---------- #

DEBUG = yes

# --------- #
# Directory #
# --------- #

LIBDIR = libft/
PATHLIBDIR = libft/
SRCDIR = src/
OBJDIR = objs/
INCDIR = inc/
INCLIBDIR = libft/

VPATH = objs:\
	src:src/hash_func:src/parser:src/process:src/tools:src/print:src/clean:\
	src/base64:src/cipher_func:src/cipher_func/password

# ------------------ #
# Compiler and flags #
# ------------------ #

CC = cc
ifeq ($(DEBUG), yes)
	CFLAGS = -Wall -Wextra -g3 -fsanitize=address
else
	CFLAGS = -Wall -Wextra -Werror -O3
endif
CPPFLAGS = -I $(INCDIR) -I $(INCLIBDIR)
LDLIBS = -lft -lm 
LDFLAGS = -L $(PATHLIBDIR)

# --------------- #
# Different names #
# --------------- #

NAME = ft_ssl
SRCS_NAMES = main.c binary_tools.c dispatcher.c hash_tools.c parser_hash.c \
			 print.c usage.c parser.c process_hash.c endian_tool.c sha512.c \
			 read_source.c utils.c md5.c md5_setup.c sha224.c sha256.c clean.c \
			 sha256_setup.c sha512_setup.c sha384.c merkel_damgard.c sha512t.c \
			 parser_base64.c base64.c process_base64.c pbkdf.c parser_cipher.c \
			 des.c process_cipher.c des_tools.c

HEADERS_NAMES = define.h dispatcher.h ft_ssl_md5.h functions.h

OBJS_NAMES = $(SRCS_NAMES:.c=.o)
LIBS_NAMES = libft.a

OBJ = $(addprefix $(OBJDIR), $(OBJS_NAMES))
HEADERS = $(addprefix $(INCDIR), $(HEADERS_NAMES))
LIBS = $(addprefix $(PATHLIBDIR), $(LIBS_NAMES))

# ----------------- #
# Command variables #
# ----------------- #

CREATE = mkdir -p
DEL = /bin/rm -rf
PRINT = printf
PHONY = all clean cleans fclean re libs cleanlibs fcleanlibs lldb norm help
REMOVE = "\r\033[K"
FUNC = "%-60b\r"

# ----- #
# Rules #
# ----- #

all : libs $(NAME)

ifeq ($(DEBUG), yes)
	@$(PRINT) "Debug mode : on\n"
else
	@$(PRINT) "Debug mode : off\n"
endif

$(NAME) : $(LIBS) $(OBJS_NAMES)
	@$(CC) -o $@ $(OBJ) $(LDFLAGS) $(LDLIBS) $(CFLAGS) $(CPPFLAGS)
	@$(PRINT) $(REMOVE)"Executable built\n"

libs :
	@$(MAKE) -j3 -C $(LIBDIR)

%.o : %.c $(HEADERS)
	@$(CREATE) $(OBJDIR)
	@$(CC) -o $(OBJDIR)$@ -c $< $(CFLAGS) $(CPPFLAGS)

clean : cleanlibs
	@$(DEL) $(OBJDIR)
	@$(PRINT) ".o file deleted\n"

cleans :
	@$(DEL) $(OBJDIR)
	@$(PRINT) ".o file deleted\n"

fclean : cleans fcleanlibs
	@$(DEL) $(NAME)
	@$(PRINT) "Executable destroyed\n"

cleanlibs :
	@$(MAKE) -C $(LIBDIR) clean

fcleanlibs :
	@$(MAKE) -C $(LIBDIR) fclean

lldb :
	@lldb ./$(NAME)

norm :
	@norminette ./$(NAME)

re : fclean all

help :
	@$(PRINT) "Rules available : all, clean, cleans, fclean, re, libs, cleanlibs, fcleanlibs, lldb, norm and help\n"

.PHONY : $(PHONY)

