#!/bin/bash

compare_no_cut() {
	echo  "$2" | openssl $1  > openssl_temp
	echo  "$2" | ./ft_ssl $1 -q > ft_ssl_temp
	var=$(diff openssl_temp ft_ssl_temp)
	if [ -n "$var" ]; then
		echo -n "X" 
		echo "ERROR ON " $2 $1 "WITH DIFF" $var
		echo $var > diff_no_cut
	else
		echo -n "Y"
	fi
	rm openssl_temp
	rm ft_ssl_temp
}

compare() {
	echo -n "$2" | openssl $1 | cut -d ' ' -f 2 > openssl_temp
	echo -n "$2" | ./ft_ssl $1 -q > ft_ssl_temp
	var=$(diff openssl_temp ft_ssl_temp)
	if [ -n "$var" ]; then
		echo -n "X" 
		echo "ERROR ON " $2 $1 "WITH DIFF" $var
		echo $var > diff
	else
		echo -n "Y"
	fi
	rm openssl_temp
	rm ft_ssl_temp
}

compare_b64() {
	echo  "$2" | openssl $1  > openssl_temp
	echo  "$2" | ./ft_ssl $1 -b 64 -q > ft_ssl_temp
	var=$(diff openssl_temp ft_ssl_temp)
	if [ -n "$var" ]; then
		echo -n "X" 
		echo "ERROR ON " $2 "($1)" 
		cp openssl_temp ssl_result
		cp ft_ssl_temp ft_result
	else
		echo -n "Y"
	fi
	rm openssl_temp
	rm ft_ssl_temp
}

testSuite() {
	echo "Testing:" $1
	var=$(openssl $1 hashTest.sh 2>&1 > /dev/null)
	if [ "$?" -eq 1 ] || [ -n "$var" ]; then 
		echo "openssl does not have "  $1 " on this computer, skipping..."
	else
		compare $1 a
		compare $1 ""
		compare $1 "abc"
		compare $1 123456
		compare $1 "Hello World"
		compare $1 "Hello Rick"
		compare $1 "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
		compare $1 "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrl mnopqrsmnopqrstnopqrstu"
		compare $1 "$\(cat Makefile\)"
		compare $1 "$\(ls src libft\)"
		compare $1 "$\(cat ft_ssl | head -c 400\)"
		compare $1 "$\(cat ft_ssl\)"
		compare $1 "$\(yes Q | tr -d \\n\ | head -c 1000)"
		compare $1 "$\(yes A | tr -d \\n\ | head -c 1000)"
		echo
	fi
}

base64test() {
	echo "Testing base64 -e"
	compare_b64 "base64 -e" a
	compare_b64 "base64 -e" ""
	compare_b64 "base64 -e" "abc"
	compare_b64 "base64 -e" 123456
	compare_b64 "base64 -e" "Hello World"
	compare_b64 "base64 -e" "Hello Rick"
	compare_b64 "base64 -e" "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
	cat /dev/urandom | head -1000 | tr '\0' '\n' > garbo
	compare_b64 "base64 -e" "$(cat garbo)" 
	rm garbo
	echo
	echo "Testing base64 -d"
	compare_b64 "base64 -d" $(echo tutu | base64)
	cat ./src/base64/base64.c | base64  > truc
	compare_b64 "base64 -d" "$(cat truc)"
	rm truc
	compare_b64 "base64 -d" "$(cat ft_ssl | base64)"
	compare_b64 "base64 -d" "$(cat /dev/urandom | base64 | head -100)"
	compare_b64 "base64 -d" "$(echo eoigheroigheroihgoeirhgeorihgoeri | base64)"
	compare_b64 "base64 -d" "$(cat auteur | base64)"
	compare_b64 "base64 -d" "$(cat /dev/urandom | base64 | head -10)"

	echo
}

testSuite md5
testSuite sha256
testSuite sha224
testSuite sha512
testSuite sha512-224
testSuite sha512-256
testSuite sha384
base64test
