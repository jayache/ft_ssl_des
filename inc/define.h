/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/21 15:31:48 by selver            #+#    #+#             */
/*   Updated: 2022/02/01 08:25:45 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINE_H
# define DEFINE_H

# include <stdint.h>

typedef enum	e_functype {
	HASH,
	BASE64,
	CIPHER
}				t_functype;

typedef uint8_t	t_flag;

typedef void		(*t_round_function)(uint64_t *, uint64_t *, uint64_t **);
typedef void		(*t_reverse_endian)(void *, void *, size_t);

typedef struct	s_md_struct
{
	size_t				buffer_size;
	size_t				worksize;
	size_t				size_of_padding_size;
	size_t				reverse_endian_param;
	uint64_t			*chunk0;
	uint64_t			**consts;
	size_t				digest_size_in_bits;
	t_round_function	round;
	t_reverse_endian	reverse_endian;
}				t_md_struct;

typedef struct	s_source {
	uint8_t	*message;
	char	*origin;
	int		fd;
	int		out_fd;
	int		break_on;
	size_t	position;
	size_t	current_size;
	size_t	total_size;
	uint8_t	padding_byte_set;
}				t_source;

typedef struct	s_command {
	char		*command_name;
	t_list		*to_crypt;
	t_flag		flags;
	char		*password;
	char		*salt;
	char		*iv;
	char		*key;
	int			out_fd;
	int			break_on;
}				t_command;

typedef union	u_m {
	uint8_t		*message;
	uint32_t	*m_32_bits;
	uint64_t	*m_64_bits;
}				t_m;

typedef struct	s_message {
	t_m			m;
	int			size_in_bits;
	uint32_t	*digest;
}				t_message;

typedef uint32_t	*(*t_hash)(t_source *, t_flag);
typedef void		(*t_parser)(t_command *, int, char **);

#endif
