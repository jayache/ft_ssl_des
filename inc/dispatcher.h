/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dispatcher.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/21 15:32:23 by selver            #+#    #+#             */
/*   Updated: 2021/02/03 09:39:50 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DISPATCHER_H
# define DISPATCHER_H

# include "functions.h"

typedef struct	s_hashfunc {
	char const	*name;
	t_hash		func;
	size_t		digest_length;
	t_functype	type;
}				t_hashfunc;

extern const t_hashfunc g_dispatcher[];

#endif
