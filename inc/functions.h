/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/21 15:32:32 by selver            #+#    #+#             */
/*   Updated: 2022/02/02 10:56:22 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FUNCTIONS_H
# define FUNCTIONS_H

# include <stdint.h>
# include "define.h"
# include "dispatcher.h"

/*
** HASH FUNCTIONS
*/

uint32_t	*ft_md5(t_source *src, t_flag flag);
uint32_t	*ft_sha256(t_source *src, t_flag flags);
uint32_t	*ft_sha224(t_source *src, t_flag flags);
uint32_t	*ft_sha512(t_source *src, t_flag flags);
uint32_t	*ft_sha384(t_source *src, t_flag flags);
uint32_t	*ft_sha512t(t_source *src, t_flag flags);
uint32_t	*ft_sha512_224(t_source *src, t_flag flags);
uint32_t	*ft_sha512_256(t_source *src, t_flag flags);
int			hash_main(t_command *command);
int			base64_main(t_command *command);
int			cipher_main(t_command *command);

/*
** MD5
*/

void		to_bytes(uint32_t val, uint8_t *bytes);
int			next_512_bits_chunk(int current);
void		setup_variables_md5(uint32_t *s, uint32_t *k, uint32_t *startchunk);
t_message	padded_message(char const *message, size_t len, size_t size,
		uint8_t byte);
t_message	mdcompliant_padding(t_source *src, t_md_struct *mdstc, size_t size);
uint32_t	*chunk_to_digest(uint32_t *chunk, size_t size,
		t_reverse_endian endian, size_t param);
/*
** SHA2
*/

void		setup_variables_sha2(uint32_t *primes, uint32_t *chunk);
void		setup_variables_sha512(uint64_t *primes, uint64_t *chunk);
t_message	padded_message_sha2(char const *mes, size_t size, size_t csize,
		uint8_t byte);
t_message	padded_message_sha512(uint8_t const *mes, size_t size, size_t csize,
		uint8_t byte);
void		reverse_endian_for_all(uint32_t *array, size_t size);
void		reverse_endian_for_all64(uint64_t *array, size_t size);
t_message	ft_merkel_damgard(t_source *src, t_flag flags,
		t_md_struct *mdstruct);
t_message	sha512(t_source *src, uint64_t *k, uint64_t *chunk0, t_flag flags);
void		sha256_round(uint64_t *mes, uint64_t *chunk0, uint64_t **k);

/*
** BASE64
*/

uint32_t	*ft_base64(t_source *src, t_flag flags);

/*
** CIPHER
*/

uint32_t	*ft_des(t_source *src, t_flag flags);
uint64_t	get_bit(uint64_t number, uint64_t bit, int numbersize);
uint64_t	get_modified_key(uint64_t key);
uint64_t	sbox(uint64_t expanded_key);

/*
** KEY GENERATION
*/

uint32_t	*pbkdf(char *password, uint8_t *cipher, t_hash md, t_flag flag);
/*
** DISPATCHER
*/

void		append_src(t_list **head, char *message, char *origin, int fd);
t_hash		get_hash_function(char *name);
size_t		get_hash_digest_size(char *name);
t_hashfunc	get_struct(char *name);

/*
** PARSER
*/

t_command	parse_command(int ac, char **av);
void		parse_hash(t_command *command, int ac, char **av);
void		parse_base64(t_command *command, int ac, char **av);
void		parse_cipher(t_command *command, int ac, char **av);
int			handle_file_errors(t_source *src, t_command *command);
uint8_t		*translate_parameters(char *hex_string);

/*
** I/O
*/

void		read_source(t_source *src, t_flag flags, size_t buffer_len);
void		print_hash_result(t_command command, uint32_t *digest, size_t size,
		t_source src);
char		*upercase(char *name);

/*
** USAGE
*/

void		usage(int extended);
void		flag_help(int type);
void		invalid_flag(char const *flag, int type);
void		invalid_command(char const *command);

/*
** BINARY
*/

uint32_t	leftrotate(uint32_t x, uint32_t c);
uint64_t	leftrotate64(uint64_t x, uint32_t c);
uint32_t	rightrotate(uint32_t word, int number);
uint64_t	rightrotate64(uint64_t word, int number);
void		sum_chunks(uint32_t *chunk0, uint32_t *chunks, size_t len);
void		sum_chunks64(uint64_t *chunk0, uint64_t *chunks, size_t len);
void		reverse_endian(void *dst, void *src, size_t size_in_word);
void		reverse_endian32(void *dst, void *src, size_t size_in_word);
void		reverse_endian64(void *dst, void *src, size_t size_in_word);

/*
** CLEAN
*/

void		free_command(t_command *command);

#endif
