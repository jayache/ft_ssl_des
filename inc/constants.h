/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   constants.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <jayache@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 07:13:55 by jayache           #+#    #+#             */
/*   Updated: 2022/01/31 09:56:21 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONSTANTS_H
# define CONSTANTS_H

# define BUFFER_MAX		64
# define FUNCTION_TYPE	3

/*
** MD5:
** FLAG_QUIET: do not print anything except digest
** FLAG_ECHO: print stdin and append digest
** FLAG_REVERSE: print origin after digest
** FLAG_STRING: hash next argument
** FLAG_COLON: separate output with colons
** BASE64:
** FLAG_ENCODE: set base64 mode
*/

# define FLAG_QUIET		1
# define FLAG_ECHO		2
# define FLAG_PRINT		2
# define FLAG_REVERSE	4
# define FLAG_STRING	8
# define FLAG_COLON		16
# define FLAG_DECODE	32
# define FLAG_NOSALT	64
# define FLAG_PWD		128

/*
** BASE64
** MODE_NONE: NO SPECIFIC ACTION
** MODE_CLEAR: CLEAR FUNCTION MEMORY AND RETURN 0
** MODE_LAST: APPEND FUNCTION MEMORY TO OUTPUT WITHOUT WAITING FOR MORE
*/

# define MODE_NONE		0 
# define MODE_CLEAR		1
# define MODE_LAST		2

# define USAGE_HASH		"Usage: ft_ssl [command] [options] [file...]\n" \
						"Options are:\n" \
						"-c:\t\toutput the digest with separating colons\n" \
						"-r:\t\treverse the format of the output\n" \
						"-h:\t\tprint this help\n" \
						"-q:\t\tquiet mode\n" \
						"-s arg:\t\tuse the given string as argument\n" \
						"-p:\t\techo STDIN to STDOUT and append the checksum to STDOUT\n" \
						"-o file:\tprint to file\n"

# define USAGE_BASE64	"Usage: ft_ssl base64 [options] [file...]\n" \
						"Options are:\n" \
						"-d:\t\tDecrypt\n" \
						"-e:\t\tEncrypt\n" \
						"-b N:\t\tBreak every N characters\n" \
						"-s arg:\t\tuse the given string as argument\n" \
						"-h:\t\tprint this help\n" \
						"-o file:\tprint to file\n"

#define BASE64_STRING "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123" \
	"456789+/"



typedef enum	e_arrayname
{
	A,
	B,
	C,
	D,
	E,
	F,
	G,
	H
}				t_arrayname;

#endif
