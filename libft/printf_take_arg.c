/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_take_arg.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/01 11:27:35 by jayache           #+#    #+#             */
/*   Updated: 2021/01/31 08:30:44 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#	include "ft_printf.h"

long long		take_int_arg(t_arg arg, va_list *ap)
{
	if (ft_strequ(arg.conversion, "l"))
		return ((long long)va_arg(*ap, long));
	if (ft_strequ(arg.conversion, "z"))
		return ((long long)va_arg(*ap, long));
	if (ft_strequ(arg.conversion, "ll"))
		return ((long long)va_arg(*ap, long long));
	if (ft_strequ(arg.conversion, "h"))
		return ((short)va_arg(*ap, int));
	if (ft_strequ(arg.conversion, "hh"))
		return ((char)va_arg(*ap, int));
	if (ft_strequ(arg.conversion, "j"))
		return (va_arg(*ap, long long));
	return ((long long)va_arg(*ap, int));
}

unsigned long	take_unsigned_arg(t_arg arg, va_list *ap)
{
	if (ft_strequ(arg.conversion, "l"))
		return ((unsigned long long)va_arg(*ap, unsigned long));
	if (ft_strequ(arg.conversion, "ll"))
		return ((unsigned long long)va_arg(*ap, unsigned long long));
	if (ft_strequ(arg.conversion, "h"))
		return ((unsigned short)va_arg(*ap, unsigned int));
	if (ft_strequ(arg.conversion, "hh"))
		return ((unsigned char)va_arg(*ap, unsigned int));
	if (ft_strequ(arg.conversion, "j"))
		return (va_arg(*ap, unsigned long long));
	if (ft_strequ(arg.conversion, "z"))
		return ((unsigned long long)va_arg(*ap, size_t));
	return ((unsigned long long)va_arg(*ap, unsigned int));
}

long double		take_double_arg(t_arg arg, va_list *ap)
{
	if (ft_strequ(arg.conversion, "L"))
		return (va_arg(*ap, long double));
	return (va_arg(*ap, double));
}
