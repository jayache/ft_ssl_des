/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/24 07:32:22 by selver            #+#    #+#             */
/*   Updated: 2021/01/24 08:43:07 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_is_prime(size_t number)
{
	size_t	i;

	i = 2;
	while (i < number)
	{
		if (number % i == 0)
			return (0);
		++i;
	}
	return (1);
}
