/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/29 22:26:21 by jayache           #+#    #+#             */
/*   Updated: 2021/01/31 08:29:52 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_print_memory(void *array, size_t size)
{
	size_t	x;

	x = 0;
	while (x < size)
	{
		if (!ft_isprint(((char*)array)[x]))
			ft_printf("[%2.2x]", ((uint8_t*)array)[x]);
		else
			ft_printf("[%2.2x]", ((uint8_t*)array)[x]);
		++x;
	}
}
