#!/bin/bash

var=$(which md5)
if [ -n "$var" ]; then
	echo "pickle rick" | md5 > md5_result
	echo "pickle rick" | ./ft_ssl md5  > ft_ssl_result

	diff md5_result ft_ssl_result

	echo "Do not pity the dead, Harry." | ./ft_ssl md5 -p > mdr
	echo "Do not pity the dead, Harry." | md5 -p > ftr

	diff mdr ftr

	echo "Pity the living." | ./ft_ssl md5 -q -r > mdr
	echo "Pity the living." | md5 -q -r > ftr
	diff mdr ftr

	echo "And above all," > file

	./ft_ssl md5 file > mdr
	md5 file > ftr
	diff mdr ftr

	./ft_ssl md5 -r file > mdr
	md5 -r file > ftr
	diff mdr ftr


	./ft_ssl md5 -s "pity those that aren't following baerista on spotify." > mdr
	md5 -s "pity those that aren't following baerista on spotify." > ftr
	diff mdr ftr

	echo "be sure to handle edge cases carefully" | ./ft_ssl md5 -p file > mdr
	echo "be sure to handle edge cases carefully" | md5 -p file > ftr
	diff mdr ftr

	echo "some of this will not make sense at first" | ./ft_ssl md5 file > mdr
	echo "some of this will not make sense at first" | md5 file > ftr
	diff mdr ftr

	echo "but eventually you will understand" | ./ft_ssl md5 -p -r file > mdr
	echo "but eventually you will understand" | md5 -p -r file > ftr
	diff mdr ftr

	echo "GL HF let's go" | ./ft_ssl md5 -p -s "foo" file > mdr
	echo "GL HF let's go" | md5 -p -s "foo" file > ftr
	diff mdr ftr

	echo "one more thing" | ./ft_ssl md5 -r -p -s "foo" file -s "bar" > mdr &> t
	echo "one more thing" | md5 -r -p -s "foo" file -s "bar" > ftr &> t
	diff mdr ftr

	echo "just to be extra clear" | ./ft_ssl md5 -r -q -p -s "foo" file > mdr
	echo "just to be extra clear" | md5 -r -q -p -s "foo" file > ftr
	diff mdr ftr

	rm mdr ftr md5_result ft_ssl_result file t
else
	echo "md5 is not on this computer, cannot compare format..."
fi
