/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/02 19:30:58 by selver            #+#    #+#             */
/*   Updated: 2021/02/05 08:28:55 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

void		sum_chunks(uint32_t *chunk0, uint32_t *chunk, size_t size)
{
	size_t	i;

	i = 0;
	while (i < size)
	{
		chunk0[i] += chunk[i];
		++i;
	}
}

void		sum_chunks64(uint64_t *chunk0, uint64_t *chunk, size_t size)
{
	size_t	i;

	i = 0;
	while (i < size)
	{
		chunk0[i] += chunk[i];
		++i;
	}
}
