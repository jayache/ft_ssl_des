/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   endian_tool.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 20:27:20 by selver            #+#    #+#             */
/*   Updated: 2021/02/10 09:50:58 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

void	reverse_endian(void *dst, void *src, size_t size_in_word)
{
	size_t	i;

	i = 0;
	while (i < size_in_word)
	{
		((uint8_t*)dst)[size_in_word - i - 1] = ((uint8_t*)src)[i];
		++i;
	}
}

void	reverse_endian32(void *dst, void *src, size_t size_in_word)
{
	size_t	i;

	i = 0;
	while (i < size_in_word)
	{
		((uint32_t*)dst)[size_in_word - i - 1] = ((uint32_t*)src)[i];
		++i;
	}
}

void	reverse_endian64(void *dst, void *src, size_t size_in_word)
{
	size_t	i;

	i = 0;
	while (i < size_in_word)
	{
		((uint64_t*)dst)[size_in_word - i - 1] = ((uint64_t*)src)[i];
		++i;
	}
}
