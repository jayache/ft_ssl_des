/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   binary_tools.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/27 07:40:44 by selver            #+#    #+#             */
/*   Updated: 2021/02/05 11:19:19 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

uint32_t	leftrotate(uint32_t x, uint32_t c)
{
	return (x << c) | (x >> (32 - c));
}

uint32_t	rightrotate(uint32_t word, int number)
{
	return ((word >> number) | (word << (32 - number)));
}

uint64_t	leftrotate64(uint64_t x, uint32_t c)
{
	return (x << c) | (x >> (64 - c));
}

uint64_t	rightrotate64(uint64_t word, int number)
{
	return ((uint64_t)(word >> number) | (uint64_t)(word << (64 - number)));
}
