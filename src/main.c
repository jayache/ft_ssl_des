/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/21 15:40:35 by selver            #+#    #+#             */
/*   Updated: 2022/01/31 09:59:37 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

int			main(int ac, char **av)
{
	t_command	command;
	int			ret;
	t_hashfunc	stct;

	command = parse_command(ac, av);
	stct = get_struct(command.command_name);
	ret = 0;
	if (stct.type == HASH)
		ret = hash_main(&command);
	else if (stct.type == BASE64)
		ret = base64_main(&command);
	else if (stct.type == CIPHER)
		ret = cipher_main(&command);
	free_command(&command);
	return (ret);
}
