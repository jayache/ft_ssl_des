/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_hash.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/03 14:11:23 by selver            #+#    #+#             */
/*   Updated: 2021/02/11 14:04:04 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

int			handle_file_errors(t_source *src, t_command *command)
{
	struct stat	buf;

	if (src->fd == -1 || src->fd > 2)
	{
		errno = 0;
		lstat(src->origin, &buf);
		if (errno)
		{
			ft_printf("%Fft_ssl: %s: %s: %s\n", 2, command->command_name,
					src->origin, strerror(errno));
			return (1);
		}
		else if (ft_is_dir(src->origin))
		{
			ft_printf("%Fft_ssl: %s: %s: is a directory\n", 2,
					command->command_name, src->origin, strerror(errno));
			return (1);
		}
	}
	return (0);
}

int			handle_command(t_source *src, t_command *command)
{
	t_hash		f;
	size_t		size;
	uint32_t	*digest;

	if (handle_file_errors(src, command))
		return (1);
	else
	{
		f = get_hash_function(command->command_name);
		size = get_hash_digest_size(command->command_name);
		digest = f(src, command->flags);
		print_hash_result(*command, digest, size, *src);
		free(digest);
		return (0);
	}
}

int			hash_main(t_command *command)
{
	t_list	*current;
	int		return_value;

	return_value = 0;
	if (!command->to_crypt || command->flags & FLAG_ECHO)
		append_src(&command->to_crypt, NULL, "stdin", 0);
	current = command->to_crypt;
	while (current)
	{
		return_value |= handle_command(current->content, command);
		current = current->next;
	}
	return (return_value);
}
