/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_cipher.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/03 14:11:23 by selver            #+#    #+#             */
/*   Updated: 2022/02/03 08:18:43 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

uint8_t		*translate_parameters(char *hex_string)
{
	uint8_t	*param;
	int		temp;
	size_t	i;

	if (strlen(hex_string) > 16)
		printf("hex string is too long, ignoring excess\n");
	param = ft_memalloc(sizeof(uint8_t) * 8);
	for (i = 0; i < strlen(hex_string) / 2 && i < 8; ++i)
	{
		sscanf(hex_string + (i * 2), "%2x", &temp);
		param[i] = (uint8_t)temp;
	}
	if (i < 8)
		printf("hex string is too short, padding with zero bytes to length\n");
	for (; i < 8; ++i)
		param[i] = 0;
	return (param);
}

static int	handle_command(t_source *src, t_command *command)
{
	t_hash		f;
	size_t		size;
	uint32_t	*digest;

	if (handle_file_errors(src, command))
		return (1);
	else
	{
		f = get_hash_function(command->command_name);
		size = get_hash_digest_size(command->command_name);
		digest = f(src, command->flags);
		//print_hash_result(*command, digest, size, *src);
		free(digest);
		return (0);
	}
}

uint32_t		*get_key(t_command *command)
{
	uint32_t	*key_and_iv;
	uint8_t		*salt = NULL;
	uint8_t		*key = NULL;

	if (!command->password)
	{
		command->password = "toto";
	}
	if (command->salt)
		salt = translate_parameters(command->salt);
	else
	{
		salt = malloc(sizeof(uint8_t) * 8);
	}
	key_and_iv = pbkdf(command->password, salt, ft_sha256, 0);
	if (command->key)
	{
		key = translate_parameters(command->key);
		ft_memcpy(key_and_iv, key, 8);
		free(key);
	}
	if (command->flags & FLAG_PRINT)
	{
		if (!(command->flags & FLAG_NOSALT))
			printf("salt=%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X\n",
					salt[0], salt[1], salt[2], salt[3], salt[3], salt[4], salt[5], salt[6], salt[7]);
		printf("key=%X%X\niv =%X%X\n", key_and_iv[0], key_and_iv[1], key_and_iv[2], key_and_iv[3]); 
	}
	free(salt);
	return (key_and_iv);
}

int			cipher_main(t_command *command)
{
	t_list	*current;
	int		return_value;

	uint32_t *key = get_key(command);
	return_value = 0;
	
	if (!command->to_crypt)
		append_src(&command->to_crypt, NULL, "stdin", 0);
	current = command->to_crypt;
	while (current)
	{
		return_value |= handle_command(current->content, command);
		current = current->next;
	}
	free(key);
	return (return_value);
}
