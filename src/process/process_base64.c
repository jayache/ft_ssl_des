/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_base64.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <jayache@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/21 07:52:34 by jayache           #+#    #+#             */
/*   Updated: 2022/02/02 10:57:02 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

static int			handle_command(t_source *src, t_command *command)
{
	t_hash		f;

	if (handle_file_errors(src, command))
		return (1);
	else
	{
		src->break_on = command->break_on;
		src->out_fd = command->out_fd;
		f = get_hash_function(command->command_name);
		f(src, command->flags);
		return (0);
	}
}

int			base64_main(t_command *command)
{
	t_list	*current;
	int		return_value;

	return_value = 0;
	if (!command->to_crypt || command->flags & FLAG_ECHO)
		append_src(&command->to_crypt, NULL, "stdin", 0);
	current = command->to_crypt;
	while (current)
	{
		return_value |= handle_command(current->content, command);
		current = current->next;
	}
	return (return_value);
}
