/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_source.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/27 13:37:39 by selver            #+#    #+#             */
/*   Updated: 2022/02/01 08:29:25 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

static void	read_stdin_term(t_source *src, size_t buffer_len)
{
	size_t	currently;

	currently = 0;
	while (currently != buffer_len)
	{
		src->current_size = read(src->fd, src->message + currently, buffer_len
				- currently);
		if (src->current_size == 0)
			break ;
		currently += src->current_size;
	}
}

void		read_source(t_source *src, t_flag flags, size_t buffer_len)
{
	if (src->message == NULL)
		src->message = ft_memalloc(buffer_len);
	if (src->fd >= 0)
	{
		ft_bzero(src->message, buffer_len);
		if (src->fd == 0 && isatty(STDIN_FILENO))
			read_stdin_term(src, buffer_len);
		else
			src->current_size = read(src->fd, src->message, buffer_len);
		if (flags & FLAG_ECHO && src->fd == 0)
			ft_printf("%s", src->message);
	}
	else
	{
		src->message = (uint8_t*)(src->origin + src->position);
		if (src->position > src->total_size)
			src->position = src->total_size;
		src->current_size = src->total_size - src->position;
		if (src->position + buffer_len > src->total_size - src->position)
			src->position = src->total_size;
		else
			src->position += buffer_len;
	}
}
