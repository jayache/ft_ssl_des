/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/03 13:56:30 by selver            #+#    #+#             */
/*   Updated: 2022/01/30 10:11:46 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

static void		print_command(t_functype filtre)
{
	size_t	x;

	x = 0;
	while (g_dispatcher[x].name)
	{
		if (g_dispatcher[x].type == filtre)
		{
			ft_printf("%F%s\n", 2, g_dispatcher[x].name);
		}
		++x;
	}
}

void			invalid_command(char const *command)
{
	if (command)
		ft_printf("%Fft_ssl: Error: '%s' is an invalid command.\n", 2, command);
	usage(!!command);
}

void			invalid_flag(char const *flag, int type)
{
	ft_printf("%Funknown option '%s'\n", 2, flag);
	flag_help(type);
}

void			flag_help(int type)
{
	if (type == HASH)
	{
		ft_printf("%F%s", 2, USAGE_HASH);
	}
	else if (type == BASE64)
	{
		ft_printf("%F%s", 2, USAGE_BASE64);
	}
	exit(EXIT_FAILURE);
}

void			usage(int extended)
{
	ft_printf("%Fusage: ft_ssl command [command opts] [command args]\n", 2);
	if (extended)
	{
		ft_printf("%FStandard commands:\n\n", 2);
		ft_printf("%FMessage Digest commands:\n", 2);
		print_command(HASH);
		ft_printf("%FCipher commands:\n", 2);
	}
	exit(EXIT_FAILURE);
}
