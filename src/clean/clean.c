/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/11 12:37:10 by selver            #+#    #+#             */
/*   Updated: 2021/02/12 08:54:00 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

void	free_src(void *source, size_t ignore)
{
	t_source *src;

	src = source;
	if (src->fd >= 0 && src->message)
		free(src->message);
	free(src);
	(void)ignore;
}

void	free_command(t_command *command)
{
	ft_lstdel(&command->to_crypt, free_src);
}
