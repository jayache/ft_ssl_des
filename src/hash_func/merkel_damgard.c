/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   merkel_damgard.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <jayache@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/10 09:51:25 by jayache           #+#    #+#             */
/*   Updated: 2021/02/12 08:53:19 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

t_message		mdcompliant_padding(t_source *src, t_md_struct *mdstruct,
		size_t size)
{
	t_message	message;

	message.size_in_bits = mdstruct->buffer_size * 8;
	message.m.message = ft_memalloc(message.size_in_bits / 8);
	ft_memcpy(message.m.message, src->message, src->current_size);
	if (!src->padding_byte_set)
		message.m.message[src->current_size] = 0x80;
	size *= 8;
	mdstruct->reverse_endian(&message.m.message[message.size_in_bits / 8 -
			mdstruct->size_of_padding_size], &size,
			mdstruct->reverse_endian_param);
	return (message);
}

t_message		ft_merkel_damgard(t_source *src, t_flag flags,
		t_md_struct *mdstruct)
{
	size_t		total;
	t_message	mes;

	total = 0;
	read_source(src, flags, mdstruct->buffer_size);
	while (src->current_size >= mdstruct->buffer_size
			- mdstruct->size_of_padding_size - 1)
	{
		if (src->current_size < mdstruct->buffer_size)
		{
			src->message[src->current_size] = 0x80;
			src->padding_byte_set = 1;
		}
		total += src->current_size;
		mdstruct->round((uint64_t*)src->message, mdstruct->chunk0,
				mdstruct->consts);
		read_source(src, flags, mdstruct->buffer_size);
	}
	total += src->current_size;
	mes = mdcompliant_padding(src, mdstruct, total);
	mdstruct->round(mes.m.m_64_bits, mdstruct->chunk0, mdstruct->consts);
	free(mes.m.message);
	mes.digest = ft_memalloc(sizeof(uint32_t) * 8);
	ft_memcpy(mes.digest, mdstruct->chunk0, sizeof(uint32_t) * 8);
	return (mes);
}
