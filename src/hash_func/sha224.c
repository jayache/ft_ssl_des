/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <jayache@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/31 08:38:14 by jayache           #+#    #+#             */
/*   Updated: 2021/02/10 09:28:06 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

uint32_t	*ft_sha224(t_source *src, t_flag flags)
{
	uint32_t	primes[65];
	uint32_t	chunk[8];
	uint32_t	*consts[1];
	t_message	padded;
	t_md_struct	mdstruct;

	consts[0] = primes;
	mdstruct = (t_md_struct){64, 4, 8, 8, (uint64_t*)chunk, (uint64_t**)consts,
		8 * 4 * 8, sha256_round, reverse_endian};
	setup_variables_sha2(primes, chunk);
	ft_memcpy(chunk, (uint32_t[]){0xc1059ed8, 0x367cd507, 0x3070dd17,
			0xf70e5939, 0xffc00b31, 0x68581511, 0x64f98fa7, 0xbefa4fa4},
			8 * sizeof(uint32_t));
	padded = ft_merkel_damgard(src, flags, &mdstruct);
	return (padded.digest);
}
