/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <jayache@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/06 08:39:19 by jayache           #+#    #+#             */
/*   Updated: 2021/02/11 07:33:46 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

uint32_t	*ft_sha384(t_source *src, t_flag flags)
{
	uint64_t	primes[80];
	uint64_t	chunk[8];
	t_message	padded;

	setup_variables_sha512(primes, chunk);
	ft_memcpy(chunk, (uint64_t[]){0xcbbb9d5dc1059ed8, 0x629a292a367cd507,
			0x9159015a3070dd17, 0x152fecd8f70e5939, 0x67332667ffc00b31,
			0x8eb44a8768581511, 0xdb0c2e0d64f98fa7, 0x47b5481dbefa4fa4},
			sizeof(uint64_t) * 8);
	padded = sha512(src, primes, chunk, flags);
	return (padded.digest);
}
