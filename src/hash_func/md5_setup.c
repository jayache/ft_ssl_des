/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_setup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/21 14:37:08 by selver            #+#    #+#             */
/*   Updated: 2021/02/10 09:49:50 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"
#include <math.h>
#include <stdio.h>
#include <stdint.h>

void		setup_variables_md5(uint32_t *s, uint32_t *k, uint32_t *start_chunk)
{
	int	i;

	i = 0;
	start_chunk[0] = 0x67452301;
	start_chunk[1] = 0xEFCDAB89;
	start_chunk[2] = 0x98BADCFE;
	start_chunk[3] = 0x10325476;
	while (i < 64)
	{
		s[i] = "\x7\xc\x11\x16\x7\xc\x11\x16\x7\xc\x11\x16\x7\xc\x11\x16"
			"\x5\x9\xe\x14\x5\x9\xe\x14\x5\x9\xe\x14\x5\x9\xe\x14"
			"\x4\xb\x10\x17\x4\xb\x10\x17\x4\xb\x10\x17\x4\xb\x10\x17"
			"\x6\xa\xf\x15\x6\xa\xf\x15\x6\xa\xf\x15\x6\xa\xf\x15"[i];
		k[i] = floor(pow(2, 32) * fabs(sin(i + 1)));
		++i;
	}
}
