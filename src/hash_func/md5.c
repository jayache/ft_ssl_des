/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/21 14:30:09 by selver            #+#    #+#             */
/*   Updated: 2021/02/11 07:10:20 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"
#include <stdio.h>
#include <stdint.h>

void		md5_functions(uint8_t i, uint32_t *f, uint32_t *g, uint32_t *chunk)
{
	if (i < 16)
	{
		*f = (chunk[B] & chunk[C]) | (~chunk[B] & chunk[D]);
		*g = i;
	}
	else if (i < 32)
	{
		*f = (chunk[D] & chunk[B]) | (~chunk[D] & chunk[C]);
		*g = (5 * i + 1) % 16;
	}
	else if (i < 48)
	{
		*f = chunk[B] ^ chunk[C] ^ chunk[D];
		*g = (3 * i + 5) % 16;
	}
	else
	{
		*f = chunk[C] ^ (chunk[B] | ~chunk[D]);
		*g = (7 * i) % 16;
	}
}

void		md5_round(uint32_t *mes, uint32_t *chunk0, uint32_t *s, uint32_t *k)
{
	uint32_t	chunk[4];
	uint32_t	g;
	uint32_t	f;
	uint32_t	i;
	uint32_t	temp;

	ft_memcpy(chunk, chunk0, 4 * sizeof(int));
	i = 0;
	while (i < 64)
	{
		md5_functions(i, &f, &g, chunk);
		temp = chunk[D];
		chunk[D] = chunk[C];
		chunk[C] = chunk[B];
		chunk[B] = chunk[B] + leftrotate(chunk[A] + f + k[i] + mes[g], s[i]);
		chunk[A] = temp;
		++i;
	}
	chunk0[A] += chunk[A];
	chunk0[B] += chunk[B];
	chunk0[C] += chunk[C];
	chunk0[D] += chunk[D];
}

uint32_t	*chunk_to_digest(uint32_t *chunk, size_t size,
		t_reverse_endian endian, size_t param)
{
	uint32_t	*digest;
	size_t		x;

	x = 0;
	digest = ft_memalloc(sizeof(uint32_t) * size);
	while (x < size)
	{
		endian(digest + x, chunk + x, param);
		++x;
	}
	return (digest);
}

void		md5_round_tl(uint64_t *mes, uint64_t *chunk0, uint64_t **sk)
{
	md5_round((uint32_t*)mes, (uint32_t*)chunk0, (uint32_t*)sk[0],
			(uint32_t*)sk[1]);
}

uint32_t	*ft_md5(t_source *src, t_flag flags)
{
	uint32_t	k[64];
	uint32_t	s[64];
	uint32_t	chunk[4];
	t_md_struct	mdst;
	t_message	m;

	mdst = (t_md_struct){64, 4, 8, 1, (uint64_t*)chunk, (uint64_t*[]){
		(uint64_t*)s, (uint64_t*)k}, 8 * 4 * 8, md5_round_tl, reverse_endian64};
	setup_variables_md5(s, k, chunk);
	m = ft_merkel_damgard(src, flags, &mdst);
	free(m.digest);
	m.digest = chunk_to_digest((uint32_t*)mdst.chunk0, 4, reverse_endian, 4);
	return (m.digest);
}
