/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512t.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <jayache@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/11 07:47:32 by jayache           #+#    #+#             */
/*   Updated: 2021/02/11 08:31:22 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

uint32_t		*ft_sha512_224(t_source *src, t_flag flags)
{
	uint64_t	primes[80];
	uint64_t	chunk[8];
	t_message	padded;

	setup_variables_sha512(primes, chunk);
	ft_memcpy(chunk, (uint64_t[]){0x8C3D37C819544DA2, 0x73E1996689DCD4D6,
			0x1DFAB7AE32FF9C82, 0x679DD514582F9FCF, 0x0F6D2B697BD44DA8,
			0x77E36F7304C48942, 0x3F9D85A86A1D36C8, 0x1112E6AD91D692A1},
			sizeof(uint64_t) * 8);
	padded = sha512(src, primes, chunk, flags);
	return (padded.digest);
}

uint32_t		*ft_sha512_256(t_source *src, t_flag flags)
{
	uint64_t	primes[80];
	uint64_t	chunk[8];
	t_message	padded;

	setup_variables_sha512(primes, chunk);
	ft_memcpy(chunk, (uint64_t[]){0x22312194FC2BF72C, 0x9F555FA3C84C64C2,
			0x2393B86B6F53B151, 0x963877195940EABD, 0x96283EE2A88EFFE3,
			0xBE5E1E2553863992, 0x2B0199FC2C85B8AA, 0x0EB72DDC81C52CA2},
			sizeof(uint64_t) * 8);
	padded = sha512(src, primes, chunk, flags);
	return (padded.digest);
}
