/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/21 15:36:39 by selver            #+#    #+#             */
/*   Updated: 2021/02/10 09:52:19 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

static void		process_chunk(uint32_t *chunk, uint32_t *k, uint32_t *w)
{
	uint32_t	temp1;
	uint32_t	s0;
	uint32_t	temp2;
	size_t		x;

	x = 0;
	while (x < 64)
	{
		temp1 = chunk[H] + ((rightrotate(chunk[E], 6) ^ rightrotate(chunk[E],
						11) ^ rightrotate(chunk[E], 25))) + ((chunk[E] &
						chunk[F]) ^ (~chunk[E] & chunk[G])) + k[x] + w[x];
		s0 = (rightrotate(chunk[A], 2) ^ rightrotate(chunk[A], 13) ^
				rightrotate(chunk[A], 22));
		temp2 = s0 + ((chunk[A] & chunk[B]) ^ (chunk[A] & chunk[C]) ^
				(chunk[B] & chunk[C]));
		chunk[H] = chunk[G];
		chunk[G] = chunk[F];
		chunk[F] = chunk[E];
		chunk[E] = chunk[D] + temp1;
		chunk[D] = chunk[C];
		chunk[C] = chunk[B];
		chunk[B] = chunk[A];
		chunk[A] = temp1 + temp2;
		++x;
	}
}

void			sha256_round(uint64_t *mes, uint64_t *chunk0, uint64_t **k)
{
	uint32_t	w[64];
	uint32_t	chunk[8];
	size_t		x;
	uint32_t	s0;
	uint32_t	s1;

	ft_bzero(w, sizeof(uint32_t) * 64);
	ft_memcpy(w, mes, sizeof(uint32_t) * 16);
	reverse_endian_for_all(w, 64);
	x = 16;
	while (x < 64)
	{
		s0 = rightrotate(w[x - 15], 7) ^ rightrotate(w[x - 15], 18) ^
			(w[x - 15] >> 3);
		s1 = rightrotate(w[x - 2], 17) ^ rightrotate(w[x - 2], 19) ^
			(w[x - 2] >> 10);
		w[x] = w[x - 16] + s0 + w[x - 7] + s1;
		++x;
	}
	ft_memcpy(chunk, chunk0, sizeof(uint32_t) * 8);
	process_chunk((uint32_t*)chunk, (uint32_t*)*k, w);
	sum_chunks((uint32_t*)chunk0, chunk, 8);
}

/*
** A comprendre: pourquoi les changements d'endian sont si chaotiques
*/

uint32_t		*ft_sha256(t_source *src, t_flag flags)
{
	uint32_t	primes[65];
	uint32_t	chunk[8];
	t_message	padded;
	t_md_struct	mdstruct;

	mdstruct = (t_md_struct){64, 4, 8, 8, (uint64_t*)chunk,
		(uint64_t*[]){(uint64_t*)primes},
		8 * 4 * 8, sha256_round, reverse_endian};
	setup_variables_sha2(primes, chunk);
	padded = ft_merkel_damgard(src, flags, &mdstruct);
	return (padded.digest);
}
