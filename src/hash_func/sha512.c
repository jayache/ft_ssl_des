/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/21 15:36:39 by selver            #+#    #+#             */
/*   Updated: 2021/02/11 12:48:27 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

static void		process_chunk(uint64_t *chunk, uint64_t *k, uint64_t *w)
{
	uint64_t	temp1;
	uint64_t	s0;
	uint64_t	temp2;
	size_t		x;

	x = 0;
	while (x < 80)
	{
		temp1 = chunk[H] + ((rightrotate64(chunk[E], 14) ^
					rightrotate64(chunk[E], 18) ^ rightrotate64(chunk[E], 41)))
			+ ((chunk[E] & chunk[F]) ^ (~chunk[E] & chunk[G])) + k[x] + w[x];
		s0 = (rightrotate64(chunk[A], 28) ^ rightrotate64(chunk[A], 34) ^
				rightrotate64(chunk[A], 39));
		temp2 = s0 + ((chunk[A] & chunk[B]) ^ (chunk[A] & chunk[C]) ^
				(chunk[B] & chunk[C]));
		chunk[H] = chunk[G];
		chunk[G] = chunk[F];
		chunk[F] = chunk[E];
		chunk[E] = chunk[D] + temp1;
		chunk[D] = chunk[C];
		chunk[C] = chunk[B];
		chunk[B] = chunk[A];
		chunk[A] = temp1 + temp2;
		++x;
	}
}

void			sha512_round(uint64_t *mes, uint64_t *k, uint64_t *chunk0)
{
	uint64_t	w[80];
	uint64_t	chunk[8];
	size_t		x;
	uint64_t	s0;
	uint64_t	s1;

	ft_bzero(w, sizeof(uint64_t) * 80);
	ft_memcpy(w, mes, sizeof(uint64_t) * 16);
	reverse_endian_for_all64(w, 80);
	x = 16;
	while (x < 80)
	{
		s0 = rightrotate64(w[x - 15], 1) ^ rightrotate64(w[x - 15], 8) ^
			(w[x - 15] >> 7);
		s1 = rightrotate64(w[x - 2], 19) ^ rightrotate64(w[x - 2], 61) ^
			(w[x - 2] >> 6);
		w[x] = w[x - 16] + s0 + w[x - 7] + s1;
		++x;
	}
	ft_memcpy(chunk, chunk0, sizeof(uint64_t) * 8);
	process_chunk(chunk, k, w);
	sum_chunks64(chunk0, chunk, 8);
}

void			restore_endian(uint64_t *chunk0)
{
	size_t		x;
	uint64_t	temp;

	x = 0;
	while (x < 8)
	{
		reverse_endian32(&temp, chunk0 + x, 2);
		chunk0[x] = temp;
		++x;
	}
}

t_message		sha512(t_source *src, uint64_t *k, uint64_t *chunk0,
		t_flag flags)
{
	size_t		total;
	t_message	mes;

	total = 0;
	read_source(src, flags, BUFFER_MAX + BUFFER_MAX);
	while (src->current_size >= BUFFER_MAX * 2 - 17)
	{
		if (src->current_size < BUFFER_MAX * 2)
		{
			src->message[src->current_size] = 0x80;
			src->padding_byte_set = 1;
		}
		total += src->current_size;
		sha512_round((uint64_t*)src->message, k, chunk0);
		read_source(src, flags, BUFFER_MAX * 2);
	}
	total += src->current_size;
	mes = padded_message_sha512(src->message, total, src->current_size,
			src->padding_byte_set);
	sha512_round(mes.m.m_64_bits, k, chunk0);
	restore_endian(chunk0);
	mes.digest = ft_memalloc(sizeof(uint64_t) * 8);
	free(mes.m.message);
	ft_memcpy(mes.digest, chunk0, sizeof(uint64_t) * 8);
	return (mes);
}

uint32_t		*ft_sha512(t_source *src, t_flag flags)
{
	uint64_t	primes[80];
	uint64_t	chunk[8];
	t_message	padded;

	setup_variables_sha512(primes, chunk);
	padded = sha512(src, primes, chunk, flags);
	return (padded.digest);
}
