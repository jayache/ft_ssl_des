/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/22 15:15:05 by selver            #+#    #+#             */
/*   Updated: 2021/02/12 08:48:22 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

int		is_from_string(t_source src)
{
	return (src.fd == -2);
}

void	print_digest(uint32_t *digest, size_t size, t_flag flags, int fd)
{
	size_t	i;
	uint8_t	b[4];

	i = 0;
	while (i < size / 4)
	{
		reverse_endian(b, (uint8_t*)(digest + i), 4);
		if (flags & FLAG_COLON)
		{
			ft_printf("%F%02x:%02x:%02x:%02x", fd, b[0], b[1], b[2], b[3]);
			if (i != (size - 1) / 4)
				ft_printf(":");
		}
		else
			ft_printf("%F%02x%02x%02x%02x", fd, b[0], b[1], b[2], b[3]);
		++i;
	}
}

void	print_name_before(t_command cmd, t_source src)
{
	if (!(cmd.flags & FLAG_QUIET) && !(cmd.flags & FLAG_REVERSE) &&
			!ft_strequ(src.origin, "stdin"))
	{
		if (is_from_string(src))
			ft_printf("%F%s (\"%s\") = ", cmd.out_fd,
					upercase(cmd.command_name), src.origin);
		else
			ft_printf("%F%s (%s) = ", cmd.out_fd, upercase(cmd.command_name),
					src.origin);
	}
}

void	print_name_after(t_command cmd, t_source src)
{
	if (cmd.flags & FLAG_REVERSE && !(cmd.flags & FLAG_QUIET) &&
			!ft_strequ(src.origin, "stdin"))
	{
		if (is_from_string(src))
			ft_printf("%F \"%s\"", cmd.out_fd, src.origin);
		else
			ft_printf("%F %s", cmd.out_fd, src.origin);
	}
}

void	print_hash_result(t_command cmd, uint32_t *digest, size_t size,
		t_source src)
{
	print_name_before(cmd, src);
	print_digest(digest, size, cmd.flags, cmd.out_fd);
	print_name_after(cmd, src);
	ft_printf("%F\n", cmd.out_fd);
}
