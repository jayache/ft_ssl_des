/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/23 15:20:30 by selver            #+#    #+#             */
/*   Updated: 2021/01/23 15:34:05 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

char	*upercase(char *name)
{
	static char buffer[20];
	int			i;

	i = 0;
	while (name[i])
	{
		buffer[i] = ft_toupper(name[i]);
		++i;
	}
	return (buffer);
}
