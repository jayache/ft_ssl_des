/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <jayache@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/20 08:10:38 by jayache           #+#    #+#             */
/*   Updated: 2022/02/02 10:57:41 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"
#include <stdio.h>

/*
 ** DECODE A BUFFER, RETURN TRANSLATION IN *DST
 ** MODE CONSTANTS ARE DEFINED IN constants.h
 ** KEEP TRACK OF UNFINISHED BASE64 QUATUOR 
 */

int		convert_buffer_from(t_source *src, uint8_t *dst, t_flag flags, int mode)
{
	static uint8_t	group[4];
	static int	current;
	int	dst_index;
	size_t	i;
	long index;
	uint32_t	res;

	dst_index = 0;
	if (mode & MODE_CLEAR)
	{
		current = 0;
		ft_bzero(group, 4);
		return (0);
	}
	if (flags & FLAG_DECODE)
	{
		i = 0;
		while (i < src->current_size)
		{
			index = (long)(strchr(BASE64_STRING, src->message[i]));
			if (index != 0)
			{
				index = index - (long)BASE64_STRING;
				group[current] = index;
				if ((char)index & 0b11000000)
					printf("WTF??? %ld\n", index);
				current++;
			}
			if (current > 3)
			{
				res = (group[0] << 18) | (group[1] << 12) | (group[2] << 6) | group[3];
				int x = 0;
				while (x < 4 * 6 / 8)
				{
					dst[dst_index++] = res >> 16;
					res = (res << 16) >> 8;
					++x;
				}
				current = 0;
				ft_bzero(group, 4);
			}
			++i;
		}
		if (mode & MODE_LAST && current > 0)
		{
			int bytes = current * 6;
			while (current < 4)
				group[current++] = 0;
			res = (group[0] << 18) | (group[1] << 12) | (group[2] << 6) | group[3];
			while (bytes > 0)
			{
				dst[dst_index++] = res >> 16;
				res = (res << 16) >> 8;
				bytes -= 6;
			}
			dst_index--;
			current = 0;
			ft_bzero(group, 4);
		}
	}
	return (dst_index);
}

void	print_converted(t_source *src, uint8_t *buffer, size_t size)
{
	for (size_t i = 0; i < size; ++i)
		ft_printf("%F%c", src->out_fd, buffer[i]);
}

void	_frombase64(t_source *src, t_flag flags)
{
	uint8_t		buffer[65];

	convert_buffer_from(src, buffer, flags, MODE_CLEAR);
	read_source(src, flags, 64);
	while (src->current_size)
	{
		ft_bzero(buffer, 65);
		int ret = convert_buffer_from(src, buffer, flags, MODE_NONE);
		print_converted(src, buffer, ret);
		read_source(src, flags, 64);
	}
	ft_bzero(buffer, 65);
	int ret = convert_buffer_from(src, buffer, flags, MODE_LAST);
	print_converted(src, buffer, ret);
}

void	print_b64(uint8_t *buffer, int *printed, t_source *src)
{
	int	i;

	i = 0;
	while (i < 4)
	{
		if (src->break_on != 0 && *printed % src->break_on == 0 && *printed > 0)
			ft_printf("%F\n", src->out_fd);
		ft_printf("%F%c", src->out_fd, buffer[i]);
		*printed += 1;
		++i;
	}
}

void	_tobase64(t_source *src, t_flag flags)
{
	size_t		i;
	uint8_t		buffer[4];
	uint32_t	v;
	int			printed;

	printed = 0;
	read_source(src, flags, 3 * 64);
	while (src->current_size)
	{
		i = 0;
		while (i < 64 && i * 3 < src->current_size)
		{
			v = src->message[i * 3];
			v = i*3+1 < src->current_size ? v << 8 | src->message[i*3+1] : v << 8;
			v = i*3+2 < src->current_size ? v << 8 | src->message[i*3+2] : v << 8;
			ft_bzero(buffer, 4);
			if (src->current_size > i * 3 + 2)
				buffer[3] = BASE64_STRING[v & 0x3F];
			else
				buffer[3] = '=';
			if (src->current_size > i * 3 + 1)
				buffer[2] = BASE64_STRING[(v >> 6) & 0x3F];
			else
				buffer[2] = '=';
			buffer[1] = BASE64_STRING[(v >> 12) & 0x3F];
			buffer[0] = BASE64_STRING[(v >> 18) & 0x3F];
			print_b64(buffer, &printed, src);
			i += 1;
		}
		read_source(src, flags, 3 * 64);
		i = 0;
	}
	if (printed > 0)
		ft_printf("%F\n", src->out_fd);
}

uint32_t	*ft_base64(t_source *src, t_flag flags)
{
	if (flags & FLAG_DECODE)
		_frombase64(src, flags);
	else
		_tobase64(src, flags);
	return (NULL);
}
