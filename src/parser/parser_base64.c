/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_base64.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <jayache@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/20 07:45:18 by jayache           #+#    #+#             */
/*   Updated: 2021/03/20 17:13:07 by selver           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

static void		flag_list(t_command *cmd, int *i, int ac, char **av)
{
	if (ft_strequ(av[*i], "-d"))
		cmd->flags |= FLAG_DECODE;
	else if (ft_strequ(av[*i], "-e"))
		cmd->flags &= ~FLAG_DECODE;
	else if (ft_strequ(av[*i], "-h"))
		flag_help(BASE64);
	else if (ft_strequ(av[*i], "-q"))
		cmd->flags |= FLAG_QUIET;
	else if (ft_strequ(av[*i], "-i"))
	{
		(*i)++;
		(*i < ac) ? append_src(&cmd->to_crypt, NULL, av[*i], open(av[*i], O_RDONLY)) : usage(1);
	}
	else if (ft_strequ(av[*i], "-o"))
	{
		(*i)++;
		(*i < ac) ? cmd->out_fd = open(av[*i], O_CREAT | O_WRONLY) : usage(1);
	}
	else if (ft_strequ(av[*i], "-s"))
	{
		cmd->flags |= FLAG_STRING;
		(*i)++;
		(*i < ac) ? append_src(&cmd->to_crypt, av[*i], av[*i], -2) : usage(1);
	}
	else if (ft_strequ(av[*i], "-b"))
	{
		(*i)++;
		if (*i < ac && ft_strisnumeric(av[*i]) && ft_strlen(av[*i]) < 4)
			cmd->break_on = ft_atoi(av[*i]);
		else
			usage(1);
	}		
	else
		invalid_flag(av[*i], HASH);
}

static int			fill_flags(t_command *command, int ac, char **av)
{
	int		i;

	i = 2;
	while (i < ac && av[i][0] == '-')
	{
		flag_list(command, &i, ac, av);
		++i;
	}
	return (i);
}

static void		parse_file(t_command *command, int ac, char **av, int i)
{
	while (i < ac)
	{
		append_src(&command->to_crypt, NULL, av[i], open(av[i], O_RDONLY));
		++i;
	}
}

void		parse_base64(t_command *command, int ac, char **av)
{
	int		i;

	i = fill_flags(command, ac, av);
	parse_file(command, ac, av, i);
}
