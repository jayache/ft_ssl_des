/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/03 13:59:02 by selver            #+#    #+#             */
/*   Updated: 2022/01/31 09:56:29 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

t_parser	command_dispatcher(int	functype)
{
	t_parser	dispatcher[FUNCTION_TYPE];
		
	ft_memcpy(dispatcher, (t_parser[]){parse_hash, parse_base64, parse_cipher},
			sizeof(t_parser) * FUNCTION_TYPE);
	return (dispatcher[functype]);
}

t_command	parse_command(int ac, char **av)
{
	t_command	command;
	t_hashfunc	func;

	ft_bzero(&command, sizeof(t_command));
	command.out_fd = 1;
	if (ac < 2 || !get_hash_function(av[1]))
		invalid_command(av[1]);
	command.command_name = av[1];
	func = get_struct(command.command_name);
	command_dispatcher(func.type)(&command, ac, av);
	return (command);
}
