/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_hash.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/22 14:34:38 by selver            #+#    #+#             */
/*   Updated: 2022/02/01 08:25:25 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

void		append_src(t_list **head, char *message, char *origin, int fd)
{
	t_source src;

	ft_bzero(&src, sizeof(t_source));
	src.message = (uint8_t*)message;
	src.origin = origin;
	src.fd = fd;
	src.position = 0;
	src.out_fd = 1;
	if (fd == -2)
		src.total_size = strlen(message);
	if (fd == 0)
		ft_lstadd(head, ft_lstnew(&src, sizeof(t_source)));
	else
		ft_lst_append(head, ft_lstnew(&src, sizeof(t_source)));
}

static void		flag_list(t_command *cmd, int *i, int ac, char **av)
{
	if (ft_strequ(av[*i], "-q"))
		cmd->flags |= FLAG_QUIET;
	else if (ft_strequ(av[*i], "-r"))
		cmd->flags |= FLAG_REVERSE;
	else if (ft_strequ(av[*i], "-p"))
		cmd->flags |= FLAG_ECHO;
	else if (ft_strequ(av[*i], "-h"))
		flag_help(HASH);
	else if (ft_strequ(av[*i], "-c"))
		cmd->flags |= FLAG_COLON;
	else if (ft_strequ(av[*i], "-out"))
	{
		(*i)++;
		(*i < ac) ? cmd->out_fd = open(av[*i], O_WRONLY) : usage(1);
	}
	else if (ft_strequ(av[*i], "-s"))
	{
		cmd->flags |= FLAG_STRING;
		(*i)++;
		(*i < ac) ? append_src(&cmd->to_crypt, av[*i], av[*i], -2) : usage(1);
	}
	else
		invalid_flag(av[*i], HASH);
}

static int			fill_flags(t_command *command, int ac, char **av)
{
	int		i;

	i = 2;
	while (i < ac && av[i][0] == '-')
	{
		flag_list(command, &i, ac, av);
		++i;
	}
	return (i);
}

static void		parse_file(t_command *command, int ac, char **av, int i)
{
	while (i < ac)
	{
		append_src(&command->to_crypt, NULL, av[i], open(av[i], O_RDONLY));
		++i;
	}
}

void		parse_hash(t_command *command, int ac, char **av)
{
	int		i;

	i = fill_flags(command, ac, av);
	parse_file(command, ac, av, i);
}
