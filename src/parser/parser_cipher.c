/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_cipher.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/22 14:34:38 by selver            #+#    #+#             */
/*   Updated: 2022/01/31 09:55:50 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

static void		flag_list(t_command *cmd, int *i, int ac, char **av)
{
	if (ft_strequ(av[*i], "-s"))
	{
		(*i)++;
		(*i < ac) ? cmd->salt = av[*i] : usage(1);
	}
	else if (ft_strequ(av[*i], "-v"))
	{
		(*i)++;
		(*i < ac) ? cmd->iv = av[*i] : usage(1);
	}
	else if (ft_strequ(av[*i], "-k"))
	{
		(*i)++;
		(*i < ac) ? cmd->key = av[*i] : usage(1);
	}
	else if (ft_strequ(av[*i], "-p"))
	{
		(*i)++;
		(*i < ac) ? cmd->password = av[*i] : usage(1);
	}
	else if (ft_strequ(av[*i], "-P"))
	{
		cmd->flags |= FLAG_PRINT;
	}
	else if (ft_strequ(av[*i], "-h"))
		flag_help(CIPHER);
	else if (ft_strequ(av[*i], "-S"))
		cmd->flags |= FLAG_NOSALT;
	else if (ft_strequ(av[*i], "-i"))
	{
		(*i)++;
		(*i < ac) ? append_src(&cmd->to_crypt, NULL, av[*i], open(av[*i], O_RDONLY)) : usage(1);
	}
	else if (ft_strequ(av[*i], "-o"))
	{
		(*i)++;
		(*i < ac) ? cmd->out_fd = open(av[*i], O_WRONLY) : usage(1);
	}
	else if (ft_strequ(av[*i], "-a"))
	{
		cmd->flags |= FLAG_STRING;
		(*i)++;
		(*i < ac) ? append_src(&cmd->to_crypt, av[*i], av[*i], -2) : usage(1);
	}
	else
		invalid_flag(av[*i], HASH);
}

static int			fill_flags(t_command *command, int ac, char **av)
{
	int		i;

	i = 2;
	while (i < ac && av[i][0] == '-')
	{
		flag_list(command, &i, ac, av);
		++i;
	}
	return (i);
}

static void		parse_file(t_command *command, int ac, char **av, int i)
{
	while (i < ac)
	{
		append_src(&command->to_crypt, NULL, av[i], open(av[i], O_RDONLY));
		++i;
	}
}

void		parse_cipher(t_command *command, int ac, char **av)
{
	int		i;

	i = fill_flags(command, ac, av);
	parse_file(command, ac, av, i);
}
