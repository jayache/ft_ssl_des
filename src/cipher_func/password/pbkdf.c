/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pbkdf.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/31 08:48:46 by jayache           #+#    #+#             */
/*   Updated: 2022/02/01 08:30:58 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

/*
** OPENSSL PASSWORD BASED KEY DERIVATION FUNCTION
** IV IS 8 BYTES AFTER KEY
*/

uint32_t	*pbkdf(char *password, uint8_t *salt, t_hash md, t_flag flags)
{
	t_source	*src_hash;
	uint32_t	*key_iv;
	char		*buf;

	buf = ft_memalloc(strlen(password) + 9);
	ft_memcpy(buf, password, strlen(password));
	ft_memcpy(buf + strlen(password), salt, 8);
	src_hash = ft_memalloc(sizeof(t_source));
	src_hash->message = buf; 
	src_hash->origin = buf;
	src_hash->total_size = ft_strlen(password) + 8;
	src_hash->fd = -2;
	key_iv = md(src_hash, 0);
	free(src_hash);
	free(buf);
	return (key_iv);
	(void)flags;
}
