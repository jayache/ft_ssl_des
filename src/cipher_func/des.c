/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/31 09:48:42 by jayache           #+#    #+#             */
/*   Updated: 2022/02/03 10:21:49 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"


void		print_to_hex(uint64_t block)
{
	uint8_t *b = (uint8_t*)&block;
	printf("%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X", b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7]);
}

void		print_expanded(uint8_t sections[8])
{
	uint64_t	block;

	block = 0;
	for (int i = 0; i < 8; ++i)
		block = ((block << 6) | sections[i]);
	print_to_hex(block);
}

uint64_t	ft_des_block(uint64_t	block, uint64_t key, t_flag flags)
{
	uint8_t		perm_table[] = {58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44,	36,	28,	20,	12,	4,
		62, 54,	46,	38,	30,	22 ,14 ,6, 64, 56, 48, 40, 32, 24, 16, 8, 57, 49, 41, 33, 25, 17, 9,
		1, 59, 51, 43, 35, 27, 19, 11, 3,61, 53, 45, 37, 29, 21, 13, 5, 63,	55,	47,	39,	31,	23,	15,	7};
	uint8_t		final_perm[] = { 40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31,
		38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29,
		36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27,
		34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25 };
	uint64_t	perm_block = 0;
	uint32_t	splits[2];
	uint8_t		exp_d[] = { 32, 1, 2, 3, 4, 5, 4, 5,
		6, 7, 8, 9, 8, 9, 10, 11,
		12, 13, 12, 13, 14, 15, 16, 17,
		16, 17, 18, 19, 20, 21, 20, 21,
		22, 23, 24, 25, 24, 25, 26, 27,
		28, 29, 28, 29, 30, 31, 32, 1 };

	int per[32] = { 16, 7, 20, 21,
		29, 12, 28, 17,
		1, 15, 23, 26,
		5, 18, 31, 10,
		2, 8, 24, 14,
		32, 27, 3, 9,
		19, 13, 30, 6,
		22, 11, 4, 25 };

	for (int i = 0; i < 64; ++i)
	{
		perm_block |= get_bit(block, perm_table[i], 64) << i;
	}

	uint64_t modified_key = get_modified_key(key);
	printf("Key: ");
	print_to_hex(key);
	printf(" -> ");
	print_to_hex(modified_key);
	printf("\n");
	return (perm_block);


	(void)flags;
}

uint32_t	*ft_des(t_source *src, t_flag flags)
{

	read_source(src, flags, 8);
	uint8_t *key = translate_parameters("133457799BBCDFF1");
	ft_des_block(*(uint64_t*)src->message, ((uint64_t*)key)[0], flags);

	free(key);
	return (NULL);
	(void)src;
	(void)flags;
}
