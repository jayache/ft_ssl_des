/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dispatcher.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: selver <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/21 15:36:00 by selver            #+#    #+#             */
/*   Updated: 2022/01/31 09:48:20 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl_md5.h"

const t_hashfunc g_dispatcher[] = {
	{ .name = "md5", .func = ft_md5, .digest_length = 16, .type = HASH},
	{"sha256", ft_sha256, 256 / 8, HASH},
	{"sha224", ft_sha224, 224 / 8, HASH},
	{"sha512", ft_sha512, 512 / 8, HASH},
	{"sha512-224", ft_sha512_224, 224 / 8, HASH},
	{"sha512-256", ft_sha512_256, 256 / 8, HASH},
	{"sha384", ft_sha384, 384 / 8, HASH},
	{"base64", ft_base64, 0, BASE64},
	{"des", ft_des, 64, CIPHER},
	{0},
};

t_hash		get_hash_function(char *name)
{
	int i;

	i = 0;
	while (g_dispatcher[i].name)
	{
		if (!ft_strcmp(g_dispatcher[i].name, name))
			return (g_dispatcher[i].func);
		++i;
	}
	return (NULL);
}

size_t		get_hash_digest_size(char *name)
{
	int i;

	i = 0;
	while (g_dispatcher[i].name)
	{
		if (!ft_strcmp(g_dispatcher[i].name, name))
			return (g_dispatcher[i].digest_length);
		++i;
	}
	return (0);
}

t_hashfunc	get_struct(char *name)
{
	size_t	i;

	i = 0;
	while (g_dispatcher[i].name)
	{
		if (!ft_strcmp(g_dispatcher[i].name, name))
			return (g_dispatcher[i]);
		++i;
	}
	return ((t_hashfunc){0});
}
